<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statu extends Model
{
    //
    public function tasks(){
		return $this->belongsToMany('App\Task','tasks_status_relationship','status_id','task_id');
    }
}
