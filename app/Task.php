<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    public function user(){
    	return $this->belongsTo('App\User','user_id','id');
    }
    public function status(){
		return $this->belongsToMany('App\Statu','tasks_status-relationship','task_id','status_id');
    }
}
