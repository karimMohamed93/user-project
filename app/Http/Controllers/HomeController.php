<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use App\Task;
use App\Statu;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user')->only(['profile','control']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function profile()
    {
        $users=DB::table('users')->get();

        return view('profile',compact('users'));
        //return view('profile');
    }

    public function control()
    {
    //     if(Auth::user()->role == 3)
    //     {
    //        return redirect('/');
    //     }
        $users=DB::table('users')->get();
        return view('control',compact('users'));
    }

     public function updateRole(Request $request, user $user)
    {
        $user->update($request->all());
        return redirect('control');
    }

     public function updateRole2(Request $request,Task $task)
    {
        $User_id=$request->input('user_id');
        if($User_id !=0){
          $task->user_id=$User_id;
          $task->save();
        }
        $task_id=$task->id;
        DB::table('tasks_status-relationship')->insert(['status_id' => 1,'task_id' => $task_id]);
        //$task->user_id = $request->input('user_id');
        //$task->save();
        return redirect('list_task');
    }

    public function submit(Request $request)
    {
        $validator = $this->validate($request,['desc'=>'required','title'=>'required']);

        if ($validator->fails()) {
            return redirect('profile')->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $Title = $request->input('title');
        $Desc = $request->input('desc');
        $user_id = $request->input('user');
            $new_Task=new Task;
            $new_Task->title=$Title;
            $new_Task->desc=$Desc;
            $new_Task->user_id=$user_id;
            $new_Task->save();

            /*$task_id=Task::find($id);
            $task_id->title=$Title;
            $task_id->desc=$Desc;
            $task_id->user_id=$user_id;
            $task_id->save();*/

            //DB::table('tasks')->where('title', $Title)->get(); 

            $task_id = DB::table('tasks')
                     ->select('id')
                     ->where('title',$Title)
                     ->first();
             //var_dump($task_id->id);
            DB::table('tasks_status-relationship')->insert(['status_id' => 1,'task_id' => $task_id->id]);
            return redirect('profile');
        
    }
    public function status(Request $request,Task $task)
    {
           $task_id=$request->input('task_id');
           $status_id=$request->input('status');
           DB::table('tasks_status-relationship')->insert(['status_id' => $status_id,'task_id' => $task_id]);
        // $task->user_id
       //return $task=Task::with('status')->get();
            // $new_Statu=new Statu;
            // $new_Statu->status_id  =$stat;
            // $new_Task->save();
        return redirect('profiles2');
        
    }

    public function profiles2()
    {
      //$users =  User::with('tasks')->where('id', Auth::id())->first();
      $status=DB::table('status')->get();
         $users= User::with(['tasks' => function($query){
          $query->with(['status'=> function($query){
            $query->get();
          }]);
      }])->where('id', Auth::id())->first();
      print_r(sizeof($users));
      return view('profiles2')->with('user',$users)->with('status',$status);
       //$users=User::with('tasks')->get()->first();
      // $user =Auth::user();
      //  $user->with('tasks')->get() ;

       //return $user;

      // return view('profiles2',compact('users','status'));
      // return view('profiles2',compact('users','status'));
    }

    public function list_task()
    {
      $status=DB::table('status')->get();
      //$users =  User::with('tasks')->get();
      $users =  User::with('tasks')->where('role',3)->get();
      //return User::with('tasks')->where('role',3)->get();
       //$users=User::with('tasks')->get()->first();
      // $user =Auth::user();
      //  $user->with('tasks')->get() ;

       //return $user;

        return view('list',compact('users','status'));
    }
    
}
