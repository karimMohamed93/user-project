<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@profile');
Route::get('/control', 'HomeController@control');
Route::post('/update-role/{user}', 'HomeController@updateRole');
Route::post('/update-role2/{task}', 'HomeController@updateRole2');
Route::post('/submit', 'HomeController@submit');
Route::post('/status/{task}', 'HomeController@status');

Route::get('/profiles2', 'HomeController@profiles2');
Route::get('/list_task', 'HomeController@list_task');