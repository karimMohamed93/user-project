-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 13, 2017 at 08:14 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `users-project`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` int(11) NOT NULL,
  `nakeName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role`, `nakeName`) VALUES
(1, 'karim', 'karimali@.com', '$2y$10$LXxgQOLFT5Egu.rBXiZHUe7AU.KX6dI6dIZfL7amuOTJ2Gc7gQYX2', NULL, NULL, NULL, 1, 'kiki'),
(2, 'mohamed', 'mohamed@.com', '$2y$10$gUk5aqir81z1r/y0lwOni.VDvqYarkl8eZuk8yAWjoy4aWUVQkX3S', NULL, NULL, NULL, 3, 'moha'),
(3, 'ahmed', 'ahmed@.com', '$2y$10$tBcbKJqPsY2kNUr1WneU1uXesXHXFSNuC0DLnINvQ9bMBfqj4ajNi', NULL, NULL, NULL, 3, 'hamada'),
(4, 'samah', 'samah@.com', '$2y$10$yO/qcZy/Kg3pNlFo/QFO4unf/IsoVJtP5AyrlC9k1K1WwniaZFMXC', NULL, NULL, NULL, 3, 'soso'),
(5, 'ali', 'ali@.com', '$2y$10$K2JuHwMHBqDFE.Q7Gf4nnenAKy/F1EitfACkpABpQCiG.qzzCA716', NULL, NULL, NULL, 2, 'aboali');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_nakename_unique` (`nakeName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
