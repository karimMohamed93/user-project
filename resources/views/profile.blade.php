@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Task</div>

                <div class="panel-body">


                    <!--@if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif 

                    Username: {{ Auth::user()->name }} 
                    <hr>
                    Email: {{ Auth::user()->email }}
                    <hr>

                   your ID: {{ Auth::user()->id }} 
                   {{ $errors->has('title') ? 'has-error' : ''}}
                   --> 



 
    

                   {{ Form::open(["url"=>"submit","files"=>"true"]) }}
                   
                        Title :{{ Form::text("title") }}
                        @if($errors->has('title'))</br>
                            <span class="label label-info">{{$errors->first('title')}}</span>
                            </br>

                         @endif
                        
                         Description: {{ Form::text("desc") }}
                         @if($errors->has('desc'))</br>
                            <span class="label label-info">{{$errors->first('desc')}}</span>

                         @endif
                         
                    <select class="form-control" name="user" > 
                       @foreach($users as $user)
                            <option value="{{$user->id}}" >{{$user->name}}</option>
                        @endforeach
                    </select>
                      {{ Form::submit("ADD",["class"=>"btn btn-info"]) }}
                    {{ Form::close() }} 


                    



                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>

@stop
