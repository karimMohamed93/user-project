@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Control Page</div>

                <div class="panel-body">

                    <div class="bs-example" data-example-id="panel-without-body-with-table">
                        <div class="panel panel-default">
                            <div class="panel-heading">panel Heading</div>

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Task_id</th>
                                        <th>Title</th>
                                        <th>description</th>
                                        <th>Assigne</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)

                                	@foreach($user->tasks as $task)
                                		<tr>
                                        	
                                        	<td>{{ $task->id}}</td>
                                        	<td>{{ $task->title}}</td>
                                        	<td>{{ $task->desc}}</td>
                                        	<td>{{ $user->name}}</td>
                                        	<td>
                                        	<div class="form-group" style="margin-bottom: 0px;">
                                 				<form method="post" action="/update-role2/{{ $task->id }}">
                                 				{{ csrf_field() }}
                                                 

	                                 				<select class="form-control" name="user_id" onchange="this.form.submit();"> 
	                                 				<option value="0">Select</option>

	                                 				@foreach($users as $newuser)
                    									<option value="{{$newuser->id}}"  {{ (($task->user_id) == ($newuser->id) ) ? 'selected' : null }}>

                    										{{$newuser->name}}
                    									</option>
                    								@endforeach
 	
                    								

           							 				</select>
           										</form>
           										</div>
       							 			</td>
                                           
                                    	</tr>

                                	@endforeach


                                    
                                 @endforeach
                                 
                                    
                                </tbody>
                                
                            </table>


                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>


@stop