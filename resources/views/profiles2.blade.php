@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Control Page</div>

                <div class="panel-body">

                    <div class="bs-example" data-example-id="panel-without-body-with-table">
                        <div class="panel panel-default">
                            <div class="panel-heading">panel Heading</div>

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>your ID</th>
                                        <th>Email</th>
                                        <th>Task_id</th>
                                        <th>Title</th>
                                        <th>description</th>
                                        
                                    </tr>
                                </thead>
                                <tbody> 
                                
                                                                

                                  @foreach($user->tasks as $task)
 

                                    
                                    <tr>
                                          <td>{{ Auth::user()->name }}</td>
                                          <td>{{ Auth::user()->id }}</td>
                                          <td>{{ Auth::user()->email }}</td>
                                          <td>{{ $task->id}}</td>
                                          <td>{{ $task->title}}</td>
                                          <td>{{ $task->desc}}</td>
                                          
                                         @foreach($task->status as $stat)
                                          @if($loop->last)
                                            @foreach($status as $statuses)

                                                
                                            
                                            


                                            <td>
                                        

                                                
                                                <form method="post" action="/status/{{ $task->id }}">
                                                {{ csrf_field() }}

                                                    <input type="hidden" name="task_id" value="{{$task->id }}">               
                                                     
                                                   <input type="radio" name="status" onchange="this.form.submit();" value="{{$statuses->id }}" 

                        {{ (($stat->name) == ($statuses->name) ) ? 'checked' : null }}
                                                     > 
                                                     
                                                    
                                                 
                                                 
                                                 {{$statuses->name}}      
                                                    

                                                        
                       
                                                        
                                                    </br>
                                                </form>
                                                
                                            </td>
                                            @endforeach 
                                            @endif
                                                
                                            @endforeach
                                             
                                      </tr>
                                    
                                
                            @endforeach
                                 
                                    
                                </tbody>
                                
                            </table>


                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>


@stop