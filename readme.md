
*  database in php my admin (users-project)

* git clone https://karimMohamed93@bitbucket.org/karimMohamed93/user-project.git.

```javascript
cd userproject
```

 * composer install
 
```javascript
composer global require "laravel/installer"
```
 
* install php

```javascript
sudo apt-get install php7.0-mysql php7.0
``` 
* install apache2
```javascript
sudo apt-get install apache
``` 
* install mysql 
```javascript
sudo apt-get install mysql-server
```



* to create and populate tables
* Create a database

```javascript
php artisan migrate
```
* Create a database

```javascript
php artisan db:seed
```
* tto start the app on http://localhost:8000/

```javascript
php artisan serve
```




