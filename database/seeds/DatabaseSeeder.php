<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        App\Task::insert(['desc'=>'ellithy motors is buy and sale the car','title'=>'ellithy motors','user_id'=>'3']);
        App\Task::insert(['desc'=>'cafe to drink cafe and tea','title'=>'cafe','user_id'=>'2']); 
        App\Task::insert(['desc'=>'hospital to Treatment','title'=>'hospital','user_id'=>'2']);
        App\Task::insert(['desc'=>'company is a good','title'=>'company','user_id'=>'3']);
        App\Task::insert(['desc'=>'to-do-list to show tasks to manager ','title'=>'to-do-list','user_id'=>'3']);
        App\User::insert(['name'=>'karim','email'=>'karimali@.com','role'=>'1','nakeName'=>'kiki','password'=>Hash::make( '1234' )]);
        App\User::insert(['name'=>'mohamed','email'=>'mohamed@.com','role'=>'3','nakeName'=>'moha','password'=>Hash::make( '654321' )]); 
        App\User::insert(['name'=>'ahmed','email'=>'ahmed@.com','role'=>'3','nakeName'=>'hamada','password'=>Hash::make( '654321' )]);
        App\User::insert(['name'=>'samah','email'=>'samah@.com','role'=>'3','nakeName'=>'soso','password'=>Hash::make( '654321' )]);
        App\User::insert(['name'=>'ali','email'=>'ali@.com','role'=>'2','nakeName'=>'aboali','password'=>Hash::make( '654321' )]);
        App\Role::insert(['role'=>'admin']);
        App\Role::insert(['role'=>'manager']);
        App\Role::insert(['role'=>'user']);
        App\Statu::insert(['name'=>'to-do']);
        App\Statu::insert(['name'=>'in progress']);
        App\Statu::insert(['name'=>'in review']);
        App\Statu::insert(['name'=>'done']);
    }
}
